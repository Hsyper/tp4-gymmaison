﻿using System;
using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using ClassGymMaison.DAL;
using ClassGymMaison.Models;
using System.Collections.Generic;

// Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=234238

namespace GymMaison
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class EntrainementFiche : Page
    {
        private int? _Id = null;
        private readonly Contexte _contexte = new Contexte();
        private int nbExerEnt = 0;
        private int ordre = 0;
        private int time = 0;
        private int index = 0;
        private int nbBoucles = 0;
        private Boolean tickFait = false;
        private Boolean tickReposFait = false;
        private Boolean countInitial = false;
        private DispatcherTimer Timer;
        private DispatcherTimer TimerRepos;
        private List<int> lstDureeExercice;

        public EntrainementFiche()
        {
            InitializeComponent();            
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.Parameter != null) _Id = (int)e.Parameter;
            if (_Id != null)
            {
                var entrainement = _contexte.Entrainements.Where(x => x.IdEntrainement == _Id).FirstOrDefault();

                if (entrainement != null)
                {
                    NomEntraintement.Text = entrainement.NomEntrainement;

                    var exercicesEntrainement = _contexte.ExercicesEntrainements.OrderBy(x => x.OrdreExercice).Where(x => x.IdEntrainement == _Id);
                    if (!countInitial)
                    {
                        nbExerEnt = _contexte.ExercicesEntrainements.Count() + 1;
                        countInitial = true;
                    }

                    /* Données de la liste d'exercices */
                    List<object> DataExercices = new List<object>();

                    foreach (ExerciceEntrainement exeEnt in exercicesEntrainement)
                    {
                        var exercice = _contexte.Exercices.FirstOrDefault(x => x.IdExercice == exeEnt.IdExercice);

                        DataExercices.Add(new
                        {
                            exeEnt.OrdreExercice,
                            exeEnt.IdExerciceEntrainement,
                            exeEnt.DureeExercice,
                            exercice.NomExercice,
                            exercice.DescriptionExercice,
                        });
                    }

                    ListExercices.ItemsSource = DataExercices;

                    var exercices = _contexte.Exercices.OrderBy(x => x.IdExercice);
                    foreach (Exercice ex in exercices)
                    {
                        Combobox_Exercices.Items.Add(ex.NomExercice);
                    }
                    ordre = exercicesEntrainement.Count();
                }
            }
        }

        private void AjouterExercice_Click(object sender, RoutedEventArgs e)
        {
            List<object> DataExercices = new List<object>();
            ExerciceEntrainement exEnt = new ExerciceEntrainement();
            var exercicesEntrainement = _contexte.ExercicesEntrainements.OrderBy(x => x.OrdreExercice).Where(x => x.IdEntrainement == _Id);
            var entrainement = _contexte.Entrainements.Where(x => x.IdEntrainement == _Id).FirstOrDefault();

            if (DureeMinutes.Text != null || DureeSecondes.Text != null)
            {
                if (Combobox_Exercices.SelectedItem != null)
                {
                    var exercice = _contexte.Exercices.Where(x => x.NomExercice == Combobox_Exercices.SelectedItem.ToString()).FirstOrDefault();

                    exEnt.OrdreExercice = ordre + 1;
                    exEnt.IdEntrainement = entrainement.IdEntrainement;
                    exEnt.DureeExercice = 0;
                    exEnt.IdExercice = exercice.IdExercice;
                    exEnt.IdExerciceEntrainement = nbExerEnt + 1;
                    if (DureeMinutes.Text != null && DureeMinutes.Text != "")
                    {
                        exEnt.DureeExercice += (Convert.ToInt32(DureeMinutes.Text) * 60);
                    }
                    if (DureeSecondes.Text != null && DureeSecondes.Text != "")
                    {
                        exEnt.DureeExercice += Convert.ToInt32(DureeSecondes.Text);
                    }
                    foreach (ExerciceEntrainement exeEnt in exercicesEntrainement)
                    {
                        var exer = _contexte.Exercices.FirstOrDefault(x => x.IdExercice == exeEnt.IdExercice);

                        DataExercices.Add(new
                        {
                            exeEnt.OrdreExercice,
                            exeEnt.IdExerciceEntrainement,
                            exeEnt.DureeExercice,
                            exer.NomExercice,
                            exer.DescriptionExercice,
                        });
                    }

                    // Ajout du nouvel ExerciceEntrainement.
                    DataExercices.Add(new
                    {
                        exEnt.OrdreExercice,
                        exEnt.IdExerciceEntrainement,
                        exEnt.DureeExercice,
                        exercice.NomExercice,
                        exercice.DescriptionExercice
                    });
                    ordre++;
                    nbExerEnt++;
                    _contexte.ExercicesEntrainements.Add(exEnt);
                    _contexte.SaveChanges();

                    Frame.Navigate(typeof(EntrainementFiche), _Id);
                }
            }
        }

        private void EffacerExercice_click(object sender, RoutedEventArgs e)
        {
            var bouton = (Button)sender;
            var id = (int)bouton.Tag;
            if (_contexte.ExercicesEntrainements.Any(x => x.IdExerciceEntrainement == id))
            {
                var ExeEnt = _contexte.ExercicesEntrainements.FirstOrDefault(x => x.IdExerciceEntrainement == id);
                if (ExeEnt != null)
                {
                    _contexte.ExercicesEntrainements.Remove(ExeEnt);
                    ordre--;
                }
                _contexte.SaveChanges();
            }
            Frame.Navigate(typeof(EntrainementFiche), _Id);
        }

        private void LancerEntrainement_click(object sender, RoutedEventArgs e)
        {
            if (NbBoucles.Text != null || NbBoucles.Text != "" || Convert.ToInt32(NbBoucles.Text) > 0) 
            {
                nbBoucles = Convert.ToInt32(NbBoucles.Text);
                lstDureeExercice = new List<int>();
                var exercicesEntrainement = _contexte.ExercicesEntrainements.OrderBy(x => x.OrdreExercice).Where(x => x.IdEntrainement == _Id);
                Timer = new DispatcherTimer();
                Timer.Interval = new TimeSpan(0, 0, 1);
                TimerRepos = new DispatcherTimer();
                TimerRepos.Interval = new TimeSpan(0, 0, 1);

                foreach (ExerciceEntrainement exEnt in exercicesEntrainement)
                {
                    lstDureeExercice.Add(exEnt.DureeExercice);   
                }
                CommencerExercice(index);                
            }
        }

        private void CommencerExercice(int index)
        {
            if (index < lstDureeExercice.Count() - 1)
            {
                if (Timer.IsEnabled || TimerRepos.IsEnabled)
                {
                    TimerRepos.Stop();
                    Timer.Stop();
                }
                if (!tickFait)
                {
                    Timer.Tick += Timer_Tick;
                    tickFait = true;
                }
                ListExercices.SelectedIndex = index;
                time = lstDureeExercice[index] + 1;
                repos.Visibility = Visibility.Collapsed;
                Timer.Start();
            } else
            {
                ListExercices.SelectedIndex = 0;
                compteur.Text = "0";
                nbBoucles--;
                index = 0;
                if (nbBoucles >= 0)
                {
                    index = 0;
                    //Repos();
                    CommencerExercice(index);
                    NbBoucles.Text = nbBoucles.ToString();
                }
            }
        }

        private void Timer_Tick(object sender, object e)
        {
            if (time > 0)
            {
                if (time <= 10)
                {
                    time--;
                    compteur.Text = string.Format("{0}:0{1}", time / 60, time % 60);
                }
                else
                {
                    time--;
                    compteur.Text = string.Format("{0}:{1}", time / 60, time % 60);
                }
            } else
            {
                Timer.Stop();
                Repos();
            }
        }

        private void Timer_Tick_Repos(object sender, object e)
        {
            if (time > 0)
            {
                if (time <= 10)
                {
                    time--;
                    compteur.Text = string.Format("{0}:0{1}", time / 60, time % 60);
                }
                else
                {
                    time--;
                    compteur.Text = string.Format("{0}:{1}", time / 60, time % 60);
                }
            }
            else
            {
                Timer.Stop();
                index++;
                CommencerExercice(index);
            }
        }
        private void Repos()
        {
            if (!tickReposFait)
            {
                TimerRepos.Tick += Timer_Tick_Repos;
                tickReposFait = true;
            }
            if (Timer.IsEnabled || TimerRepos.IsEnabled)
            {
                TimerRepos.Stop();
                Timer.Stop();
            }
            if (lstDureeExercice.Count() - 1 > index)
            {
                ListExercices.SelectedIndex = index + 1;
            }
            repos.Visibility = Visibility.Visible;
            time = 11;
            TimerRepos.Start();
        }
    }
}
