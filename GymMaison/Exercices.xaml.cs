﻿using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using ClassGymMaison.DAL;
using System.Collections.Generic;
using ClassGymMaison.Models;

// Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=234238

namespace GymMaison
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class Exercices : Page
    {
        private readonly Contexte _contexte = new Contexte();

        public Exercices()
        {
            InitializeComponent();
            var exercices = _contexte.Exercices.ToList();
            ListesExercices.ItemsSource = exercices;
            var categories = _contexte.Categories.OrderBy(x => x.IdCategorie);
            foreach (Categorie categ in categories)
            {
                Combobox_Categories.Items.Add(categ.NomCategorie);
            }

        }

        private void Exercices_SelectionChanged(object sender, RoutedEventArgs e)
        {
            var categorieFiltre = Combobox_Categories.SelectedItem.ToString();
            var exercices = _contexte.Exercices.OrderBy(x => x.IdExercice).Where(x => x.Categories.Contains(categorieFiltre));
            ListesExercices.ItemsSource = exercices;
        }
    }
}
