﻿using Windows.ApplicationModel.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace GymMaison
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            mainContent.Navigate(typeof(Entrainements), null);
        }


        private void btnQuit_Click(object sender, RoutedEventArgs e)
        {
            CoreApplication.Exit();
        }

        private void Nav_Calendrier(object sender, RoutedEventArgs e)
        {
            //** TODO **//
        }

        private void Nav_Entrainement(object sender, RoutedEventArgs e)
        {
            mainContent.Navigate(typeof(Entrainements), null);
        }

        private void Nav_Exercices(object sender, RoutedEventArgs e)
        {
            mainContent.Navigate(typeof(Exercices), null);
        }
    }
}
