﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ClassGymMaison.Models
{
    public class Exercice 
    {
        [Key]
        public int IdExercice { get; set; }
        public string NomExercice { get; set; }
        public string DescriptionExercice { get; set; }
        [ForeignKey("Categorie")]
        public string Categories { get; set; }
    }
}
