﻿using System.ComponentModel.DataAnnotations;

namespace ClassGymMaison.Models
{
    public class Categorie
    {
        [Key]
        public int IdCategorie { get; set; }
        public string NomCategorie { get; set; }
    }
}
